<?php

namespace App\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiWebTestCaseTest extends WebTestCase
{
    public function testGetToken()
    {
        $this->assertNotEmpty($this->getToken());
    }

    public function testGetTokenWithInvalidCredentials()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([
                'username' => 'xxx',
                'password' => 'xxx',
            ])
        );

        $answer = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('401', $client->getResponse()->getStatusCode());
        $this->assertTrue(is_array($answer));
        $this->assertEquals(401, $answer['code']);
        $this->assertEquals('Invalid credentials.', $answer['message']);

    }

    protected function getToken(): string
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([
                'username' => 'admin@gmail.com',
                'password' => 'admin',
            ])
        );

        $content = $client->getResponse()->getContent();
        $data    = json_decode($content, true);

        return $data['token'];
    }
}
