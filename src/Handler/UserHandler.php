<?php

namespace App\Handler;

use App\Bus\Command\UserSendEmailCommand;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\Messenger\MessageBusInterface;

class UserHandler extends BaseHandler
{
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(UserManager $manager, MessageBusInterface $bus)
    {
        $this->manager = $manager;
        $this->bus     = $bus;
    }

    public function getManager(): UserManager
    {
        return $this->manager;
    }

    public function create(User $user): User
    {
        return $this->manager->create($user);
    }

    public function update(User $user, array $parameters = []): User
    {
        return $this->manager->update($user, array_filter($parameters));
    }

    public function sendEmailToCandidate(User $user, string $message): bool
    {
        $this->bus->dispatch(
            new UserSendEmailCommand($user, $message)
        );

        return true;
    }
}
