<?php

declare(strict_types=1);

namespace App\Serializer\Naming;

use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Naming\PropertyNamingStrategyInterface;

final class MyCamelCaseNamingStrategy implements PropertyNamingStrategyInterface
{
    /**
     * @var string
     */
    private $separator;

    /**
     * @var bool
     */
    private $lowerCase;

    public function __construct(string $separator = '_', bool $lowerCase = true)
    {
        $this->separator = $separator;
        $this->lowerCase = $lowerCase;

    }

    /**
     * {@inheritDoc}
     */
    public function translateName(PropertyMetadata $property): string
    {
        $name = preg_replace('/[A-Z]+/', $this->separator . '\\0', $property->name);
        $name = str_replace(['-', '_',], '', $name);

        if ($this->lowerCase) {
            return strtolower($name);
        }

        return $name;
    }
}
