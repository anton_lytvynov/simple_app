<?php

namespace App\DTO;

use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

class ApiSWGPagination
{

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $last;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $current;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $numItemsPerPage;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $first;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $pageCount;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $totalCount;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $pageRange;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $startPage;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $endPage;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $next;

    /**
     * @var array
     *
     * @Type("array")
     * @Groups({"display", "list"})
     */
    private $pagesInRange;

    /**
     * @var float
     *
     * @Type("float")
     * @Groups({"display", "list"})
     */
    private $firstPageInRange;

    /**
     * @var float
     *
     * @Type("float")
     * @Groups({"display", "list"})
     */
    private $lastPageInRange;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $currentItemCount;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $firstItemNumber;

    /**
     * @var int
     *
     * @Type("int")
     * @Groups({"display", "list"})
     */
    private $lastItemNumber;

    public function getLast(): int
    {
        return $this->last;
    }

    public function setLast(int $last): self
    {
        $this->last = $last;

        return $this;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }

    public function setCurrent(int $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getNumItemsPerPage(): int
    {
        return $this->numItemsPerPage;
    }

    public function setNumItemsPerPage(int $numItemsPerPage): self
    {
        $this->numItemsPerPage = $numItemsPerPage;

        return $this;
    }

    public function getFirst(): int
    {
        return $this->first;
    }

    public function setFirst(int $first): self
    {
        $this->first = $first;

        return $this;
    }

    public function getPageCount(): int
    {
        return $this->pageCount;
    }

    public function setPageCount(int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): self
    {
        $this->totalCount = $totalCount;

        return $this;
    }

    public function getPageRange(): int
    {
        return $this->pageRange;
    }

    public function setPageRange(int $pageRange): self
    {
        $this->pageRange = $pageRange;

        return $this;
    }

    public function getStartPage(): int
    {
        return $this->startPage;
    }

    public function setStartPage(int $startPage): self
    {
        $this->startPage = $startPage;

        return $this;
    }

    public function getEndPage(): int
    {
        return $this->endPage;
    }

    public function setEndPage(int $endPage): self
    {
        $this->endPage = $endPage;

        return $this;
    }

    public function getNext(): int
    {
        return $this->next;
    }

    public function setNext(int $next): self
    {
        $this->next = $next;
        return $this;
    }

    public function getPagesInRange(): array
    {
        return $this->pagesInRange;
    }

    public function setPagesInRange(array $pagesInRange): self
    {
        $this->pagesInRange = $pagesInRange;

        return $this;
    }

    public function getFirstPageInRange(): float
    {
        return $this->firstPageInRange;
    }

    public function setFirstPageInRange(float $firstPageInRange): self
    {
        $this->firstPageInRange = $firstPageInRange;

        return $this;
    }

    public function getLastPageInRange(): float
    {
        return $this->lastPageInRange;
    }

    public function setLastPageInRange(float $lastPageInRange): self
    {
        $this->lastPageInRange = $lastPageInRange;

        return $this;
    }

    public function getCurrentItemCount(): int
    {
        return $this->currentItemCount;
    }

    public function setCurrentItemCount(int $currentItemCount): self
    {
        $this->currentItemCount = $currentItemCount;

        return $this;
    }

    public function getFirstItemNumber(): int
    {
        return $this->firstItemNumber;
    }

    public function setFirstItemNumber(int $firstItemNumber): self
    {
        $this->firstItemNumber = $firstItemNumber;

        return $this;
    }

    public function getLastItemNumber(): int
    {
        return $this->lastItemNumber;
    }

    public function setLastItemNumber(int $lastItemNumber): self
    {
        $this->lastItemNumber = $lastItemNumber;

        return $this;
    }
}
