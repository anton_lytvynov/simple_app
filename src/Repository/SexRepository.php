<?php

namespace App\Repository;

use App\Entity\Sex;
use App\Request\Filter\SexListFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Sex|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sex|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sex[]    findAll()
 * @method Sex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SexRepository extends EntityRepository
{
    public function findByFilter(SexListFilter $filter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');

        return $qb;
    }
}
