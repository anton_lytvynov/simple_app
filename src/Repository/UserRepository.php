<?php

namespace App\Repository;

use App\Entity\User;
use App\Request\Filter\UserListFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository
{
    public function findByFilter(UserListFilter $filter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u')->leftJoin('u.group', 'g');

        if (null !== $filter->getRole()) {
            $qb->andWhere('g.role = :role')->setParameter('role', $filter->getRole());
        }

        return $qb;
    }
}
