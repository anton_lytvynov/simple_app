<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractBaseRepository extends ServiceEntityRepository
{
    /**
     * @var string
     */
    private $entityClass;

    /**
     * @param RegistryInterface $registry
     * @param string $entityClass
     */
    public function __construct(RegistryInterface $registry, string $entityClass)
    {
        $this->entityClass = $entityClass;
        parent::__construct($registry, $entityClass);
    }

    /**
     * Returns the base part of the constructor.
     * Example of usage: Can be used for paginating
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getBaseQuery()
    {
        return $this->createQueryBuilder('e');
    }
}
