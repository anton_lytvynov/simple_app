<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Table(name="user_group")
 * @ORM\Entity()
 */
class Group
{

    const ROLE_USER      = 'ROLE_USER';
    const ROLE_ADMIN     = 'ROLE_ADMIN';
    /**
     * @var User[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User", cascade={"persist", "remove", "merge"}, mappedBy="group")
     */
    protected $users;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Type("string")
     * @Groups({"display"})
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var string
     *
     * @Type("string")
     * @Groups({"display"})
     *
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
    private $role;

    public function __construct(string $role)
    {
        $this->role  = $role;
        $this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param string $name
     *
     * @return Group
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return Group
     */
    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function __toString(): string
    {
        return sprintf('%s', $this->id);
    }
}
