<?php

namespace App\Controller;

use App\Form\LoginType;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Bus\Command\UserActivateCommand;
use App\Bus\Command\UserCreateCommand;
use App\Bus\Command\UserEncodePasswordCommand;
use App\Bus\Command\UserResetPasswordCommand;
use App\DTO\PasswordReset;
use App\Entity\Group;
use App\Entity\User;
use App\Form\RCType;
use App\Form\RPType;
use App\Form\USetPType;
use App\Security\AppUserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class SecurityController extends AbstractController
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/login", name="app_login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $form  = $this->createForm(LoginType::class);

        return $this->render('security/login.html.twig',
            [
                'error' => $error,
                'form'  => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/signup", name="app_sign_up")
     *
     * @param Request                   $request
     * @param GuardAuthenticatorHandler $authenticatorHandler
     * @param AppUserAuthenticator      $appUserAuthenticator
     *
     * @throws \Throwable
     * @return Response
     */
    public function register(Request $request, GuardAuthenticatorHandler $authenticatorHandler, AppUserAuthenticator $appUserAuthenticator)
    {
        $form = $this->createForm(RCType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $this->userManager->create($user);

            $this->addFlash('success', 'You are registered.');

            return $authenticatorHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $appUserAuthenticator,
                'main'
            );

        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
