<?php

namespace App\Controller;

use App\Entity\User;
use App\Manager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(
        UserManager $userManager
    ) {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/dashboard", name="app_dashboard")
     * @Security("has_role('ROLE_USER')")
     *
     * @return Response
     */
    function dashboard()
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->render('dashboard/dashboard.html.twig',
            [
                'user'  => $user,
                'users' => $this->userManager->getAll(),
            ]
        );
    }
}
