<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\User;
use App\Manager\UserManager;
use App\Request\Filter\UserListFilter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Rest\View(serializerGroups={"display"}, serializerEnableMaxDepthChecks=true)
 */
class UserController extends AbstractFOSRestController
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Display User
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Get(
     *     path="/api/user/{id}",
     *     summary="Get User",
     *     operationId="api_user_display",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="200",
     *          description="Successful",
     *          @SWG\Schema(
     *              type="object", ref=@Model(type=App\Entity\User::class, groups={"display"})
     *          )
     *     )
     * )
     * @Rest\Get("/api/user/{id}", name="api_user_display")
     *
     * @ParamConverter("user", class="App\Entity\User")
     *
     * @param User $user
     *
     * @return User
     */
    public function display(User $user): User
    {
        if (!$this->haveIAccessToUser($user)) {
            throw new AccessDeniedHttpException('Not allowed');
        }

        return $user;
    }

    /**
     * List Users
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @SWG\Get(
     *     path="/api/user",
     *     summary="Get Users",
     *     operationId="api_user_list",
     *     tags={"User"},
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          type="integer",
     *          description="number of page",
     *          required=false
     *     ),
     *     @SWG\Parameter(
     *          name="page_size",
     *          in="query",
     *          type="integer",
     *          description="page size",
     *          required=false
     *     ),
     *     @SWG\Parameter(
     *         description="Filters",
     *         in="query",
     *         name="filters[orderByCreatedAt]",
     *         required=false,
     *         type="string",
     *         enum={"ASC", "DESC"}
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful",
     *         @SWG\Schema(
     *             @SWG\Property(property="data",
     *                 @SWG\Items(ref=@Model(type=App\Entity\User::class, groups={"display"}))
     *             ),
     *             @SWG\Property(property="pagination", type="object", ref=@Model(type=App\DTO\ApiSWGPagination::class, groups={"display"}))
     *             )
     *         )
     *     )
     * )
     * @Rest\Get("/api/user", name="api_user_list")
     *
     * @Rest\QueryParam(name="page", nullable=true, description="Pagination number")
     * @Rest\QueryParam(name="page_size", nullable=true, description="Page size")
     * @Rest\QueryParam(name="filters", description="filters")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return array|Response|User[]
     */
    public function list(ParamFetcherInterface $paramFetcher): array
    {
        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter);

        /** @var UserListFilter $filter */
        $filter = $normalizer->denormalize($paramFetcher->get('filters'), UserListFilter::class);

        $page     = intval($paramFetcher->get('page')) ?? 1;
        $pageSize = intval($paramFetcher->get('page_size')) ?? 10;

        /** @var User[] $users */
        $users = $this->userManager->getUsersApi($filter, $page, $pageSize);

        return $users;
    }

    /**
     * Create User
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @SWG\Post(
     *     path="/api/user",
     *     summary="Create User",
     *     tags={"User"},
     *     @SWG\Parameter(
     *         description="User object",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(
     *             type="Object",
     *              @SWG\Property(property="email", type="string", example="contact@gmail.com"),
     *              @SWG\Property(property="password", type="string", example="Password12345!")
     *              )
     *         )
     *     ),
     * @SWG\Response(
     *          response="200",
     *          description="Successful",
     *          @Model(type=App\Entity\User::class, groups={"display"})
     *     ),
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * @SWG\Response(
     *         response=404,
     *         description="Not found"
     *     )
     * )
     * @Rest\Post("/api/user", name="api_user_create")
     *
     * @ParamConverter("user", class="App\Entity\User", converter="fos_rest.request_body")
     *
     * @param User $user
     *
     * @return User
     * @throws \Exception
     */
    public function create(User $user): User
    {
        return $this->userManager->create($user);
    }

    /**
     * Edit User
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Put(
     *     path="/api/user/{id}",
     *     summary="Update User",
     *     tags={"User"},
     *     @SWG\Parameter(
     *         description="User object",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(
     *             type="Object",
     *              @SWG\Property(property="email", type="string", example="contact@gmail.com")
     *              )
     *         )
     *     ),
     * @SWG\Response(
     *          response="200",
     *          description="Successful",
     *          @Model(type=App\Entity\User::class, groups={"display"})
     *     ),
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * @SWG\Response(
     *         response=404,
     *         description="Not found"
     *     )
     * )
     * @Rest\Put("/api/user/{id}", name="api_user_update")
     *
     * @RequestParam(name="email", description="Email", strict=true, map=false, nullable=false)
     *
     * @ParamConverter("userDB", class="App\Entity\User")
     *
     * @param User                  $user
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return User|Response
     * @throws \Exception
     */
    public function update(User $user, ParamFetcherInterface $paramFetcher)
    {
        if (!$this->haveIAccessToUser($user)) {
            throw new AccessDeniedHttpException('Not allowed');
        }

        return $this->userManager->update($user, $paramFetcher->get('email'));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Delete(
     *     path="/api/user/{id}",
     *     summary="Delete User",
     *     tags={"User"},
     * @SWG\Response(
     *          response="200",
     *          description="Successful"
     *     ),
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * @SWG\Response(
     *         response=404,
     *         description="Not found"
     *     )
     * )
     * @Rest\Delete("/api/user/{id}", name="api_user_delete")
     *
     * @ParamConverter("user", class="App\Entity\User")
     *
     * @param User $user
     *
     * @return bool|Response
     * @throws \Exception
     */
    public function delete(User $user): bool
    {
        if (!$this->haveIAccessToUser($user)) {
            throw new AccessDeniedHttpException('Not allowed');
        }

        $this->userManager->delete($user);

        return true;
    }

    private function haveIAccessToUser(User $user): bool
    {
        /** @var User $me */
        $me = $this->getUser();

        if ($me->isAdmin()) {
            return true;
        }

        return $me->getId() == $user->getId();
    }
}
