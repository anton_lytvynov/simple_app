<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\User;
use App\Manager\UserManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Rest\View(serializerGroups={"display"}, serializerEnableMaxDepthChecks=true)
 */
class AuthController extends AbstractFOSRestController
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Create User
     *
     * @SWG\Post(
     *     path="/api/register",
     *     summary="Register User",
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *         description="User object",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(
     *             type="Object",
     *              @SWG\Property(property="email", type="string", example="contact@gmail.com"),
     *              @SWG\Property(property="password", type="string", example="Password12345!")
     *              )
     *         )
     *     ),
     * @SWG\Response(
     *          response="200",
     *          description="Successful",
     *          @Model(type=App\Entity\User::class, groups={"display"})
     *     ),
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * @SWG\Response(
     *         response=404,
     *         description="Not found"
     *     )
     * )
     * @Rest\Post("/api/register", name="api_user_register")
     *
     * @ParamConverter("user", class="App\Entity\User", converter="fos_rest.request_body")
     *
     * @param User $user
     *
     * @return User
     * @throws \Exception
     */
    public function register(User $user): User
    {
        return $this->userManager->create($user);
    }
}
