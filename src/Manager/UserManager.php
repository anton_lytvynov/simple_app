<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Group;
use App\Entity\User;
use App\Request\Filter\UserListFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager extends AbstractBaseManager
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $className,
        SerializerInterface $serializer,
        PaginatorInterface $pagination,
        EventDispatcherInterface $eventDispatcher,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->encoder = $encoder;
        parent::__construct($entityManager, $className, $serializer, $pagination, $eventDispatcher);
    }

    /**
     * @param UserListFilter $filter
     * @param int|null       $page
     * @param int|null       $size
     * @param bool           $paginate
     *
     * @return User[]|array
     */
    public function getUsersApi(UserListFilter $filter, int $page = null, int $size = null, bool $paginate = true)
    {
        /** @var QueryBuilder $query */
        $query = $this->repository->findByFilter($filter);

        if ($paginate) {
            return $this->paginationResponse(
                $this->makePaginator($query, $page, $size, ['wrap-queries' => true, 'distinct' => false])
            );
        }

        return $query->getQuery()->execute();
    }

    /**
     * @return Collection|User[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    public function create(User $user): User
    {
        $encoded = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);

        /** @var Group $group */
        $group = $this->entityManager->getRepository(Group::class)->findOneBy(['role' => Group::ROLE_USER]);
        $user->setGroup($group);

        $this->save($user, true);

        return $user;
    }

    public function update(User $user, string $email): User
    {
        $user->setEmail($email);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function delete(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}
