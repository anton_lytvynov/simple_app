<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Sex;
use App\Repository\SexRepository;
use App\Request\Filter\SexListFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SexManager extends AbstractBaseManager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        string $className,
        SerializerInterface $serializer,
        PaginatorInterface $pagination,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct($entityManager, $className, $serializer, $pagination, $eventDispatcher);
    }

    /**
     * @param SexListFilter $filter
     * @param int|null     $page
     * @param int|null     $size
     * @param bool         $paginate
     *
     * @return Sex[]|array
     */
    public function getSexApi(SexListFilter $filter, int $page = null, int $size = null, bool $paginate = true)
    {
        /** @var SexRepository $repository */
        $repository = $this->getRepository();

        /** @var QueryBuilder $query */
        $query = $repository->findByFilter($filter);

        if ($paginate) {
            return $this->paginationResponse($this->makePaginator($query, $page, $size, ['wrap-queries' => true, 'distinct' => false]));
        }

        return $query->getQuery()->execute();
    }
}
