<?php
declare(strict_types=1);

namespace App\Manager;

use App\Repository\AbstractBaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\QueryBuilder;
use JMS\Serializer\SerializerInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class AbstractBaseManager
{
    public const PAGINATION_LIMIT = 5;
    public const PAGINATION_FIRST_PAGE = 1;

    /**
     * @var AbstractBaseRepository|ObjectRepository
     */
    protected $repository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $className;

    /**
     * @var PaginatorInterface|SlidingPagination
     */
    protected $pagination;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * BaseManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $className
     * @param SerializerInterface $serializer
     * @param PaginatorInterface $pagination
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        string $className,
        SerializerInterface $serializer,
        PaginatorInterface $pagination,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository($className);
        $this->serializer = $serializer;
        $this->pagination = $pagination;
        $this->className = $className;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param QueryBuilder $query
     * @param int|null $page
     * @param int|null $size
     * @param array $options
     * @return SlidingPagination
     */
    public function makePaginator(QueryBuilder $query, int $page = null, int $size = null, array $options = []): SlidingPagination
    {
        $paginated = $this->pagination->paginate(
            $query,
            empty($page) ? self::PAGINATION_FIRST_PAGE : $page,
            empty($size) ? self::PAGINATION_LIMIT : $size,
            $options
        );

        return $paginated;
    }

    public function getRepository(): ObjectRepository
    {
        return $this->repository;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }


    /**
     * Persist the entity.
     *
     * @param object $entity
     * @param bool $doFlush
     *
     * @return object $entity
     */
    public function save($entity, bool $doFlush = true)
    {
        $this->entityManager->persist($entity);

        if ($doFlush) {
            $this->entityManager->flush();
        }

        return $entity;
    }

    /**
     * Delete the given entity.
     *
     * @param object $entity An entity instance
     */
    public function remove($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * @param SlidingPagination $paginator
     *
     * @return array
     */
    public function paginationResponse(SlidingPagination $paginator): array
    {
        return [
            'data' => $paginator->getItems(),
            'pagination' => $paginator->getPaginationData(),
        ];
    }
}
