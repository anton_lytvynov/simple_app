<?php
declare(strict_types=1);

namespace App\Request\Filter;

use App\Traits\DataLoader;

class UserListFilter
{
    use DataLoader;

    private const ORDER_BY_OPTIONS = ['ASC', 'DESC'];

    /**
     * @var string
     */
    private $role;

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
