<?php

namespace App\Form;

use App\DTO\PasswordReset;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class USetPType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, [
                'invalid_message' => 'The password fields must match.',
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'Set a password',
                    ]
                ],
                'second_options' => [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'Set a password second time',
                    ]
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => PasswordReset::class,
                'validation_groups' => ['Default', 'PasswordReset'],
                'csrf_protection' => false,
            ]
        );
    }
}
