<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * User Certificate Upload (Video & Certificate)
 */
class UCUType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('certificate', FileType::class, [
                    'multiple' => false,
                    //'label_format' => 'ad.fields.add_file',
                    'attr'         => [
                        //'class' => 'ad-image-upload',
                    ]
                ]
            )->setMethod('POST');

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                //'data_class' => AdImage::class,
            ]
        );
    }
}
