<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class REType extends AbstractType
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'What\'s your name?',
                    ],
                    'required'     => true,
                ]
            )
            ->add('email', EmailType::class,
                [
                    //'label_format' => 'user.email',
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'What\'s your email?',
                    ],
                    'required'     => true,
                ]
            )->add('whatsApp', TextType::class,
                [
                    //'label_format' => 'user.email',
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'What\'s Your WhatsApp Number?',
                    ],
                    'required'     => true,
                ]
            )
            ->add('company', TextType::class,
                [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'What\'s your company\'s name?',
                    ],
                    'required'     => false,
                ]
            )->add('site', TextType::class,
                [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'What\'s your company\'s website?',
                    ],
                    'required'     => false,
                ]
            )
            ->add('password', PasswordType::class, [
                    'attr' => [
                        'class'       => 'form-control h-55',
                        'placeholder' => 'Set a password',
                    ],
                    'required'     => true,
                ]
            );

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class'        => User::class,
                'validation_groups' => ['Default', 'Registration'],
                'csrf_protection' => false,
            ]
        );
    }
}
