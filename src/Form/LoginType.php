<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Enter your email',
                ],
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Enter your password',
                ],
            ]);
    }
}
