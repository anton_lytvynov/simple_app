<?php

namespace App\DataFixtures;

use App\Entity\Group;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GroupFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadGroups($manager);
    }

    private function loadGroups(ObjectManager $manager)
    {
        foreach ($this->getData() as $every) {
            $group = (new Group($every[1]))
                ->setName($every[0]);
            
            $manager->persist($group);
            $this->setReference($group->getRole(), $group);
        }

        $manager->flush();
    }

    private function getData(): array
    {
        return [
            ['Admin', Group::ROLE_ADMIN,],
            ['User', Group::ROLE_USER,],
        ];
    }

    public static function getGroups(): array
    {
        return ['dev',];
    }
}
