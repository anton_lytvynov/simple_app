<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const NB_USERS = 5;

    private $faker;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(
        UserPasswordEncoderInterface $encoder
    ) {
        $this->faker   = \Faker\Factory::create();
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadAdmin($manager);
        $this->loadUsers($manager);

        $manager->flush();
    }

    private function loadAdmin(ObjectManager $manager)
    {
        $admin = (new User())
            ->setGroup($this->getAdminGroup())
            ->setEmail('admin@gmail.com')
            ->setPassword('admin');
        $admin->setPassword($this->encoder->encodePassword($admin, $admin->getPassword()));

        $manager->persist($admin);
    }

    private function loadUsers(ObjectManager $manager)
    {
        for ($i = 1; $i <= self::NB_USERS; $i++) {
            $user = (new User())
                ->setGroup($this->getGroupByRole(Group::ROLE_USER))
                ->setEmail(sprintf('user%s@gmail.com', $i))
                ->setPassword(sprintf('user%s', $i));
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));

            $manager->persist($user);

            $this->setReference(sprintf('user-%s', $i), $user);
        }
    }

    private function getAdminGroup(): Group
    {
        /** @var Group $group */
        $group = $this->getReference(Group::ROLE_ADMIN);

        return $group;
    }

    private function getGroupByRole(string $role): Group
    {
        /** @var Group $group */
        $group = $this->getReference($role);

        return $group;
    }

    public static function getGroups(): array
    {
        return ['dev',];
    }

    public function getDependencies()
    {
        return [
            GroupFixtures::class,
        ];
    }
}
