import '../css/base.scss';

import $ from 'jquery';
global.$ = global.jQuery = $;
import 'bootstrap';
import 'popper.js';
import '@fortawesome/fontawesome-free';
import Vue from 'vue';
import VueResource from 'vue-resource'
import Homepage from './components/Homepage.vue'
import { BootstrapVue } from 'bootstrap-vue';

Vue.use(VueResource);
Vue.use(BootstrapVue);

new Vue({
    'el': '#app',
    components: {Homepage}
});

//require('../images/logo.png');
