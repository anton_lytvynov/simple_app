# Simple App

## Install
```bash
composer install
php bin/console -V;
php bin/console doctrine:schema:drop --full-database --force;
php bin/console doctrine:migrations:status;
php bin/console doctrine:migrations:migrate -n;
php -d memory_limit=-4048M bin/console doctrine:fixtures:load --env=dev --append --group=dev;
yarn install;
```

## Run locally
```bash
symfony server:start
npm run dev-server --hot
```

## Credentials
* admin@gmail.com / admin / ROLE_ADMIN
* user1@gmail.com / user1 / ROLE_USER
* user2@gmail.com / user2 / ROLE_USER
* user3@gmail.com / user3 / ROLE_USER
* user4@gmail.com / user4 / ROLE_USER
* user5@gmail.com / user5 / ROLE_USER

## Api documentation
the api documentation is accessible throw /api/doc.
![Api documentation](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/api_doc.png)   
It\'s possible to play with Api directly here. We only need to enter the token.   
![Api authorise](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/api_authorise.png)   
![Api authorise2](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/api_authorise2.png)   

Than we can click on "Try it out button".      
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/api_try_response.png)

## Postman
Also I add the Postman collection & Local environment   
[Download](https://bitbucket.org/anton_lytvynov/simple_app/src/master/doc/postman/)   
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/postman.png)   


* Collection : Simple App
* Environment : Simple App - Local   
In the collection we can see needed requests to cover routes. 
If the fixtures passed well, so the postman runner should show 22 success tests.      
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/postman_runner.png)   

## Tests
```bash
php bin/phpunit tests 
```

## Webpack & Vue
Installed the webpack to get bootstrap styles for login   
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/login.png)   
   
Dashboard ROLE_USER   
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/dashboard_role_user.png)
   
Dashboard ROLE_ADMIN   
![response](https://bitbucket.org/anton_lytvynov/simple_app/raw/31c993ed2cfe3bcc4bd95c7d36686815498af60a/doc/images/dashboard_role_admin.png)

##### Contact
- Anton Lytvynov <lytvynov.anton@gmail.com>
